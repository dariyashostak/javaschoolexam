package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        validateInputNumbers(inputNumbers);

        int height = (((int) Math.sqrt(inputNumbers.size() * 8 + 1)) - 1) / 2;
        int weight = height + height - 1;
        int[][] pyramid = new int[height][weight];
        Collections.sort(inputNumbers);

        int start = 1 + weight / 2;
        int numberIndex = 0;

        for (int i = 0; i < pyramid.length; i++) {
            int current = start - 1;
            for (int j = 0; j <= i; j++) {
                pyramid[i][current] = inputNumbers.get(numberIndex);
                current += 2;
                ++numberIndex;
            }
            --start;
        }
        return pyramid;
    }

    private void validateInputNumbers(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.isEmpty() ||
                Math.sqrt(inputNumbers.size() * 8 + 1) % 1 != 0) {
            throw new CannotBuildPyramidException();
        }
    }
}
