package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.*;

public class Calculator {

    private static final Set<Character> operators;
    private static final NumberFormat formatter = NumberFormat.getNumberInstance(Locale.ENGLISH);

    static {
        operators = new HashSet<>();
        operators.add('+');
        operators.add('-');
        operators.add('/');
        operators.add('*');
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid,
     * rounding is performed to 4 significant digits
     */
    public String evaluate(String statement) {
        Deque<Double> numbers = new LinkedList<>();
        Deque<Character> operators = new LinkedList<>();
        try {
            validateStatement(statement);
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (c == '(') {
                    operators.add(c);
                } else if (c == ')') {
                    while (operators.getLast() != '(') {
                        arithmeticOperations(numbers, operators.removeLast());
                    }
                    operators.removeLast();
                } else if (isOperator(c)) {
                    if (!operators.isEmpty() && getPriority(c) <= getPriority(operators.getLast())) {
                        arithmeticOperations(numbers, operators.removeLast());
                    }
                    operators.add(c);
                } else {
                    StringBuilder s = new StringBuilder();
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) ||
                            statement.charAt(i) == '.')) {
                        s.append(statement.charAt(i++));
                    }
                    --i;
                    numbers.add(Double.parseDouble(s.toString()));
                }
            }
            while (!operators.isEmpty()) {
                arithmeticOperations(numbers, operators.removeLast());
            }
        } catch (NumberFormatException | ArithmeticException | UnsupportedOperationException e) {
            return null;
        }
        double result = numbers.getLast();
        return format(result, 4, 0);
    }

    private static boolean isParenthesisMatch(String statement) {
        Deque<Character> stack = new LinkedList<>();

        char c;
        for (int i = 0; i < statement.length(); i++) {
            c = statement.charAt(i);

            if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                if (stack.isEmpty()) {
                    return false;
                } else if (stack.peek() == '(') {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    private static boolean isOperator(char c) {
        return operators.contains(c);
    }

    private static boolean isOperatorsAndSignsCorrect(String statement) {
        int lastOperatorIndex = 0;
        for (int i = 1; i < statement.toCharArray().length; i++) {
            Character character = statement.toCharArray()[i];
            if (isOperator(character) || character == '.') {
                if (i == 1 || lastOperatorIndex + 1 != i) {
                    lastOperatorIndex = i;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    private static int getPriority(Character c) {
        if (c == '+' || c == '-') {
            return 1;
        } else if (c == '*' || c == '/') {
            return 2;
        } else if (c == '(' || c == ')') {
            return 0;
        } else {
            return -1;
        }
    }

    private static void arithmeticOperations(Deque<Double> numbers, char operator) {
        double num1 = numbers.removeLast();
        double num2 = numbers.removeLast();

        switch (operator) {
            case '+':
                numbers.add(num1 + num2);
                break;
            case '-':
                numbers.add(num2 - num1);
                break;
            case '/':
                if (num1 != 0) {
                    numbers.add(num2 / num1); //there is no problems, when we use int
                } else {
                    throw new ArithmeticException();
                }
                break;
            case '*':
                numbers.add(num1 * num2);
                break;
        }
    }

    private static void validateStatement(String statement) {
        if (statement == null || statement.isEmpty() ||
                !isParenthesisMatch(statement) || !isOperatorsAndSignsCorrect(statement)) {
            throw new UnsupportedOperationException();
        }
    }

    private static String format(double d, int maxFractionDigits, int minFractionDigits) {
        formatter.setMaximumFractionDigits(maxFractionDigits);
        formatter.setMinimumFractionDigits(minFractionDigits);
        return formatter.format(d);
    }
}
